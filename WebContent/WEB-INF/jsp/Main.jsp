<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.List,java.util.ArrayList,model.Booth" %>
<%
List<Booth> boothList = (List<Booth>) request.getAttribute("booth");

%>
<!DOCTYPE html>
<html>
<head>
<meta charset=UTF-8">
<link rel="stylesheet" href="css/style.css">
<title>メイン画面</title>
</head>
<body>
<header>
	<div class="wrap">
	<h1>メイン画面</h1>
	<nav>
		<ul class="header-menu">
			<li><a href="/production/index.jsp">スタッフ登録</a></li>
			<li><a href="/production/GuestRegister">顧客登録</a></li>
			<li><a href="/production/Logout">ログアウト</a></li>
		</ul>
	</nav>
	<h2 id="time"></h2>
	</div>
	<script>
		time();
		function time(){
			var now = new Date();
			document.getElementById("time").innerHTML = now.toLocaleString();
		}
		setInterval('time()',1000);
	</script>
</header>

<section>

	<h2>ブース状況</h2>
	<div class="wrap">
	<% for(int i=0; i<boothList.size(); i++){ %>
	<form method="get" name="form1" action="/production/Enter">
		<div class="booth">
		<input type="hidden" name="num" value="<%= i+1 %>">
        <%if(boothList.get(i).getGuest() != null){ %>
        	<input type="hidden" name="used" value="使用中" class="used">
        <% }else{ %>
        	<input type="hidden" name="canuse" value="使用可能" class="can">
        <% } %>
		<button type="submit" class="button">ブースNo.<%= i+1 %><br>
		使用状況<%= boothList.get(i).getGuest() %>
		</button>
		</div>
    </form>
	<% } %>
	</div>
</section>

<jsp:include page="/WEB-INF/jsp/footer.jsp" />
</body>
</html>