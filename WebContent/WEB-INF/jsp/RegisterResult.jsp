<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="model.StaffAccount" %>
<%
//セッションスコープからスタッフ情報を取得
StaffAccount sAccount = (StaffAccount)session.getAttribute("loginStaff");
%>
<!DOCTYPE html >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ネットカフェ業務システム</title>
</head>
<body>
<h1>スタッフ新規登録の認証結果</h1>
<% if( sAccount!= null){ %>
	<p>登録に成功しました</p>
	<p><%=sAccount.getName() %>さんの登録に成功</p>
	<a href="/production/index.jsp">TOPへ</a>
<% }else{%>
	<p>登録に失敗しました</p>
	<a href="/production/">TOPへ</a>
<% }%>

<jsp:include page="footer.jsp" />
</body>
</html>