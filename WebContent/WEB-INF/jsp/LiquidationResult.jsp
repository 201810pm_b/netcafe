<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ page import="model.Change" %>
    <%
    //リクエストスコープからインスタンスを取得
    Change change = (Change)request.getAttribute("change");
    %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<h1>清算結果</h1>
	<h2>お釣り:<%= change.getChange() %>円</h2>
</body>
</html>