package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.BoothDAO;
import dao.GuestDAO;
import model.Booth;
import model.Guest;

/**
 * Servlet implementation class EnterResult
 */
@WebServlet("/EnterResult")
public class EnterResult extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//リクエストパラメータの取得(顧客ID)
		request.setCharacterEncoding("UTF-8");
		String id = request.getParameter("id");

		//セッションスコープの取得
		HttpSession session = request.getSession();

		//Enter(servlet)からのsessionスコープからインスタンスの取得
		Booth boothNum = (Booth)session.getAttribute("result");

		//sessionスコープのインスタンスから席番号を取得
		int number = boothNum.getNumber();

		//BoothDAOインスタンスの生成
		BoothDAO boothDAO = new BoothDAO();

		//DBのBoothテーブルに上書き(テーブルのデータ：席番号と種類は既に入った状態になっている)
		boolean result = boothDAO.changeGuest(number, id);

		//上書きが出来たかどうか判別させる
		if(result){

			//Guestインスタンスの生成
			Guest guest = new Guest(id);

			GuestDAO guestDAO = new GuestDAO();

			//DBのGuestテーブルに入店時間を入れる
			boolean result2 = guestDAO.enterTime(guest);

			if(result2){ //boothテーブル・顧客テーブル両方に上書きできた場合のみフォワード

				RequestDispatcher dispatcher =
						request.getRequestDispatcher
						("WEB-INF/jsp/Main.jsp");
					dispatcher.forward(request,response);
			}

		}

	}

}
