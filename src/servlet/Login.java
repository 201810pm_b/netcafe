package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.LoginLogic;
import model.PostStaffAccountLogic;
import model.StaffAccount;

@WebServlet("/Login")
public class Login extends HttpServlet{
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response)
	throws ServletException,IOException{

		request.setCharacterEncoding("UTF-8");
		//リクエストパラメータを取得
		String name = request.getParameter("name");
		String pass = request.getParameter("pass");
		String staffId = request.getParameter("id");

		//スタッフインスタンスの作成
		StaffAccount staffAccount = new StaffAccount(staffId,name,pass);
		StaffAccount staffAccountD = new StaffAccount(staffId,name);

		//ログインロジックでパスワードの判定(1234)
		LoginLogic loginLogic = new LoginLogic();
		boolean isLogin = loginLogic.execute(staffAccount);

		//判定がtureの処理
		if(isLogin){
			PostStaffAccountLogic postSALogic = new PostStaffAccountLogic();
			postSALogic.execute(staffAccountD);//スタッフの名前とIDだけをデータベースに登録

			//ユーザー情報をセッションスコープに保存
			HttpSession session = request.getSession();
			session.setAttribute("loginStaff", staffAccountD);
		}

		//ログイン結果画面にフォワード
		RequestDispatcher dispatcher =
				request.getRequestDispatcher(
				"WEB-INF/jsp/RegisterResult.jsp");
		dispatcher.forward(request,response);
	}


}
