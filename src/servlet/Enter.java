package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.BoothDAO;
import model.Booth;

@WebServlet("/Enter")
public class Enter extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//リクエストパラメータの取得
				request.setCharacterEncoding("UTF-8");
				String num = request.getParameter("num");
				int number = Integer.parseInt(num);
				//String type = request.getParameter("type");

				//ブースDAO、インスタンスを生成
				BoothDAO boothDAO = new BoothDAO();

				//席番号から使用中か確認するメソッドを利用、resultに代入
				Booth result = boothDAO.usedCheck(number);

				//使用中かどうかで画面遷移先の振り分け、nullの際はブース使用可能
				if(result == null){

					//セッションスコープに席番号を持ったインスタンスを保存し、次のコントローラで使えるようにする
					HttpSession session = request.getSession();
					session.setAttribute("result",result);

					//フォワード
					RequestDispatcher dispatcher =
						request.getRequestDispatcher
						("WEB-INF/jsp/Enter.jsp");
					dispatcher.forward(request,response);
				}else{
					//使用中の場合
					RequestDispatcher dispatcher =
							request.getRequestDispatcher
							("WEB-INF/jsp/Exit.jsp");
						dispatcher.forward(request,response);
				}
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

}
