package servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.BoothDAO;
import model.Booth;
import model.LoginLogic;
import model.LoginStaff;
import model.StaffAccount;


@WebServlet("/Main")
public class Main extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//フォワード
		RequestDispatcher dispatcher =
				request.getRequestDispatcher("/WEB-INF/jsp/Main.jsp");
		dispatcher.forward(request,response);


	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//Booth作成とリクエストスコープへの保存
		BoothDAO boothDAO = new BoothDAO();
		List<Booth> boothList = new ArrayList<Booth>();
		boothList = boothDAO.findAll();

		//リクエストスコープにインスタンスを保存
		request.setAttribute("booth", boothList);

		//リクエストパラメータの取得
		request.setCharacterEncoding("UTF-8");
		String id = request.getParameter("id");
		String pass = request.getParameter("pass");

		//PASSだけを持つインスタンスの生成
		LoginStaff loginStaff = new LoginStaff(pass);
		//パスワードの認証
		LoginLogic loginLogic = new LoginLogic();
		boolean isLogin = loginLogic.mainExecute(loginStaff);
		//IDだけを持つスタッフインスタンスの生成
		StaffAccount sAccount = new StaffAccount(id);
		//IDをデータベースと照合
		boolean result = loginLogic.idExecute(sAccount);
		//IDの認証とPWの認証
		if(isLogin){//PWの認証
			if(result){//データベースとの認証
				//セッションスコープに保存
				HttpSession session = request.getSession();
				session.setAttribute("id", "pass");
				//フォワード
				RequestDispatcher dispatcher =
						request.getRequestDispatcher("/WEB-INF/jsp/Main.jsp");
				dispatcher.forward(request,response);
			}else{//PWが違う場合、リダイレクト
				response.sendRedirect("/production/");
			}
		}else{//IDが違う場合、リダイレクト
			response.sendRedirect("/production/");
		}

	}


}
