package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.GuestDAO;
import model.Guest;


@WebServlet("/GuestRegister")
public class GuestRegister extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request,HttpServletResponse response)
	throws ServletException, IOException {
		RequestDispatcher dispatcher =
				request.getRequestDispatcher("WEB-INF/jsp/GuestRegister.jsp");
		dispatcher.forward(request, response);
	}

	//GuestRegister.jspより顧客情報を受け取る
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response)
					throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		//リクエストパラメータを取得
		String id = request.getParameter("id");
		String name = request.getParameter("name");

		//顧客インスタンスの生成
		Guest guest = new Guest(id,name);
		GuestDAO guestDAO = new GuestDAO();

		//DBに既存のIDと重複を照合する為の顧客インスタンスを生成
		Guest check = new Guest(id);

		//重複IDがテーブルに無いかチェックする
		Guest result = guestDAO.findByLoginID(check);

		//DBに重複したIDが存在しなければnullで返ってくるのでif文で分ける
		if(result.equals(null)){

			//DBに重複IDが存在しないのでDBに登録処理をする
			guestDAO.create(guest);

			//登録が完了したのでMainにフォワード
			RequestDispatcher dispatcher =
					request.getRequestDispatcher("/Main");

			dispatcher.forward(request,response);

		//重複したIDが存在した場合GuestRegisterにフォワードする
		}else{

			RequestDispatcher dispatcher =
					request.getRequestDispatcher(
					"WEB-INF/jsp/GuestRegister.jsp");
			dispatcher.forward(request,response);
		}
	}

}
