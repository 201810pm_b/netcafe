package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.GuestDAO;
import model.CalcLogic;
import model.Change;
import model.Guest;
import model.Price;

/**
 * Servlet implementation class exitServlet
 */
@WebServlet("/exitServlet")
public class exitServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Exit.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Price price = new Price();
		//guestインスタンス取得
		HttpSession session = request.getSession();
		Guest guest = (Guest) session.getAttribute("guest");
		//リクエストパラメータ取得でExit.jspか、Liquidation.jspか判定する
		request.setCharacterEncoding("UTF-8");
		String hidden = request.getParameter("exit");
		//Exit.jspの場合
		if(hidden == "0"){
			GuestDAO gDAO = new GuestDAO();
			//退出時間をデータベースにアップデート
			boolean exitBool = gDAO.exitTime(guest);
			if(exitBool){
				//guestの全ての情報を取得
				Guest g = gDAO.findByLogoutID(guest);
				//代金計算
				CalcLogic cl = new CalcLogic();
				price = cl.execute(g.getEnterTime(), g.getExitTime());
				//priceをリクエストスコープに保存
				request.setAttribute("price", price);
				//フォワード
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Liquidation.jsp");
				dispatcher.forward(request, response);
			}
		//Liquidation.jspの場合
		}else{
			//お釣りの計算
			String money = request.getParameter("money");
			int m = Integer.parseInt(money);
			int c = m - price.getPrice();
			Change change = new Change(c);
			//changeをリクエストスコープに保存
			request.setAttribute("change", change);
			//フォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/LiquidationResult.jsp");
			dispatcher.forward(request, response);
		}
	}

}
