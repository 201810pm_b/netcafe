package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.StaffAccount;

public class StaffAccountDAO {
	Connection conn = null;
    final String DRIVER_NAME = "com.mysql.jdbc.Driver";//MySQLドライバ
    final String DB_URL = "jdbc:mysql://localhost:3306/";//DBサーバー名
    final String DB_NAME = "production";//データベース名
    final String DB_ENCODE = "?useUnicode=true&characterEncoding=utf8";//文字化け防止
    final String DB_USER = "root";//ユーザーID
    final String DB_PASS = "root";//パスワード

    public List<StaffAccount> findAll(){
    	List<StaffAccount> saList = new ArrayList<StaffAccount>();
		try {
			//JDBCドライバの読み込み
			Class.forName(DRIVER_NAME);
			//データベース、ユーザーID、パスワード読み込み
			conn = DriverManager.getConnection(
					DB_URL + DB_NAME+DB_ENCODE , DB_USER, DB_PASS);

			//SELECT文でスタッフテーブルからID,NAMEを準備
			String sql =  "SELECT id,name FROM staff";
			PreparedStatement pStmt = conn.prepareStatement(sql);
			//SELECTを実行し、結果表を取得
			ResultSet rs = pStmt.executeQuery();

			//SELECT文の結果をArrayListに格納
			while(rs.next()){
				String staffId = rs.getString("id");
				String name = rs.getString("name");
				StaffAccount staffAccount = new StaffAccount(staffId,name);
				saList.add(staffAccount);
			}

		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
			return null;
		}finally{
			if(conn != null){
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return saList;
    }

    //スタッフアカウントからDBに保存
    public boolean create(StaffAccount staffAccountD){
    	Connection conn = null;
    	try {
    		Class.forName(DRIVER_NAME);
    		//データベースへ接続
			conn = DriverManager.getConnection(
					DB_URL + DB_NAME+DB_ENCODE , DB_USER, DB_PASS);
			//INSERT文の準備
			String sql = "INSERT INTO staff(id,name)VALUES(?,?)";
			PreparedStatement pStmt = conn.prepareStatement(sql);
			//INSERT文中の「?」に使用する値を設定しSQLを完成
			pStmt.setString(1, staffAccountD.getStaffId());
			pStmt.setString(2, staffAccountD.getName());
			//INSERT文を実行
			int result = pStmt.executeUpdate();
			if(result != 1){
				return false;
			}
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
			return false;
		}finally{
			//データベース切断
			if(conn != null){
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
    	return true;
    }

//IDからスタッフアカウントの情報を取得
    public StaffAccount findByLoginID(StaffAccount staffAccount){
    	Connection conn = null;
    	StaffAccount sAccount = null;
    	try {
			Class.forName(DRIVER_NAME);
			conn = DriverManager.getConnection(
					DB_URL + DB_NAME+DB_ENCODE , DB_USER, DB_PASS);

			//SELECT文の準備
			String sql = "SELECT id FROM staff WHERE id = ?";
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, staffAccount.getStaffId());

			//SELECTを実行し、結果表を取得
			ResultSet rs = pStmt.executeQuery();

			//一致したIDが存在した場合
			if(rs.next()){
				String StaffId = rs.getString("id");

				//一致したらsAccountを生成、他はnullで返す
				sAccount = new StaffAccount(StaffId);
			}
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
			return null;
		}finally{
			//データベースの切断
			if(conn != null){
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
    	return sAccount;
    }
}
