package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import model.Guest;

public class GuestDAO {

	Connection conn = null;
    final String DRIVER_NAME = "com.mysql.jdbc.Driver";//MySQLドライバ
    final String DB_URL = "jdbc:mysql://localhost:3306/";//DBサーバー名
    final String DB_NAME = "production";//データベース名
    final String DB_ENCODE = "?useUnicode=true&characterEncoding=utf8";//文字化け防止
    final String DB_USER = "root";//ユーザーID
    final String DB_PASS = "root";//パスワード

    public List<Guest> findAll(){ //DBより顧客情報(id,name)の取得

    	//GuestクラスをArrayList格納し生成
    	List<Guest> guest = new ArrayList<Guest>();

    	try {
			//JDBCドライバの読み込み
			Class.forName(DRIVER_NAME);

			//データベース、ユーザーID、パスワード読み込み
			conn = DriverManager.getConnection(
					DB_URL + DB_NAME+DB_ENCODE , DB_USER, DB_PASS);

			//SELECT文でguestテーブルからID,NAMEを準備
			String sql =  "SELECT id,name,islogin FROM guest";
			PreparedStatement pStmt = conn.prepareStatement(sql);

			//SELECTを実行し、結果表を取得
			ResultSet rs = pStmt.executeQuery();

			//SELECT文の結果をArrayListに格納
			while(rs.next()){
				String guestId = rs.getString("id");
				String guestName = rs.getString("name");
				Guest guest1 = new Guest(guestId,guestName);
				guest.add(guest1);
			}
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		} finally {
			if(conn != null){
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
    	return guest;
    }

    //顧客情報登録時に使うメソッド
    public boolean create(Guest guest){
    	Connection conn = null;
    	try {
    		Class.forName(DRIVER_NAME);
    		//データベースへ接続
			conn = DriverManager.getConnection(
					DB_URL + DB_NAME+DB_ENCODE , DB_USER, DB_PASS);
			//INSERT文の準備
			String sql = "INSERT INTO guest(id,name)VALUES(?,?)";
			PreparedStatement pStmt = conn.prepareStatement(sql);
			//INSERT文中の「?」に使用する値を設定しSQLを完成
			pStmt.setString(1, guest.getId());
			pStmt.setString(2, guest.getName());
			//INSERT文を実行
			int result = pStmt.executeUpdate();
			if(result != 1){
				return false;
			}
    	} catch (SQLException | ClassNotFoundException e) {
    		e.printStackTrace();
    		return false;
    	} finally {
    		//データベース切断
    		if(conn != null){
    			try {
    				conn.close();
    			} catch (SQLException e) {
    				e.printStackTrace();
    			}
    		}
    	}
    	return true;
    }

    //顧客IDから顧客情報(name)の取得
    public Guest findByLoginID(Guest guest){
    	Connection conn = null;
    	Guest g = null;
    	try {
			Class.forName(DRIVER_NAME);
			conn = DriverManager.getConnection(
					DB_URL + DB_NAME+DB_ENCODE , DB_USER, DB_PASS);
			//SELECT文の準備
			String sql = "SELECT id FROM guest WHERE id = ?";
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, guest.getId());

			//SELECTを実行し、結果表を取得
			ResultSet rs = pStmt.executeQuery();

			//一致したIDが存在した場合
			if(rs.next()){
				String guestId = rs.getString("id");

				//一致したらgを生成、他はnullで返す
				g = new Guest(guestId);
			}
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
			return null;
		}finally{
			//データベースの切断
			if(conn != null){
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	    	return g;
    }
    //顧客IDから顧客情報(name)の取得
    public Guest findByLogoutID(Guest guest){
    	Connection conn = null;
    	Guest g = null;
    	try {
			Class.forName(DRIVER_NAME);
			conn = DriverManager.getConnection(
					DB_URL + DB_NAME+DB_ENCODE , DB_USER, DB_PASS);
			//SELECT文の準備
			String sql = "SELECT id,name,login@,logout@ FROM guest WHERE id = ?";
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, guest.getId());

			//SELECTを実行し、結果表を取得　
			ResultSet rs = pStmt.executeQuery();

			//一致したIDが存在した場合
			if(rs.next()){
				String guestId = rs.getString("id");
				String guestName = rs.getString("name");
				String enter = rs.getString("login@");
				String exit = rs.getString("logout@");
				//一致したらgを生成、他はnullで返す
				g = new Guest(guestId,guestName,enter,exit);
			}
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
			return null;
		}finally{
			//データベースの切断
			if(conn != null){
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	    	return g;
    }
    //顧客情報登録時に使うメソッド
    public boolean enterTime(Guest guest){
    	Date now = new Date();
    	SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    	String s = sdf.format(now);
    	Connection conn = null;
    	try {
    		Class.forName(DRIVER_NAME);
    		//データベースへ接続
			conn = DriverManager.getConnection(
					DB_URL + DB_NAME+DB_ENCODE , DB_USER, DB_PASS);
			//UPDATE文の準備
			String sql = "update guest set login@ = ? WHERE id = ?";
			PreparedStatement pStmt = conn.prepareStatement(sql);
			//UPDATE文中の「?」に使用する値を設定しSQLを完成
			pStmt.setString(1, s);
			pStmt.setString(2, guest.getId());
			//UPDATE文を実行
			int result = pStmt.executeUpdate();
			if(result != 1){
				return false;
			}
    	} catch (SQLException | ClassNotFoundException e) {
    		e.printStackTrace();
    		return false;
    	} finally {
    		//データベース切断
    		if(conn != null){
    			try {
    				conn.close();
    			} catch (SQLException e) {
    				e.printStackTrace();
    			}
    		}
    	}
    	return true;
    }
    //顧客情報登録時に使うメソッド
    public boolean exitTime(Guest guest){
    	Date now = new Date();
    	SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    	String s = sdf.format(now);
    	Connection conn = null;
    	try {
    		Class.forName(DRIVER_NAME);
    		//データベースへ接続
			conn = DriverManager.getConnection(
					DB_URL + DB_NAME+DB_ENCODE , DB_USER, DB_PASS);
			//UPDATE文の準備
			String sql = "update guest set logout@ = ? WHERE id = ?";
			PreparedStatement pStmt = conn.prepareStatement(sql);
			//UPDATE文中の「?」に使用する値を設定しSQLを完成
			pStmt.setString(1, s);
			pStmt.setString(2, guest.getId());
			//UPDATE文を実行
			int result = pStmt.executeUpdate();
			if(result != 1){
				return false;
			}
    	} catch (SQLException | ClassNotFoundException e) {
    		e.printStackTrace();
    		return false;
    	} finally {
    		//データベース切断
    		if(conn != null){
    			try {
    				conn.close();
    			} catch (SQLException e) {
    				e.printStackTrace();
    			}
    		}
    	}
    	return true;
    }
}
