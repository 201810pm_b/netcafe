package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.Booth;

public class BoothDAO {
	Connection conn = null;
    final String DRIVER_NAME = "com.mysql.jdbc.Driver";//MySQLドライバ
    final String DB_URL = "jdbc:mysql://localhost:3306/";//DBサーバー名
    final String DB_NAME = "production";//データベース名
    final String DB_ENCODE = "?useUnicode=true&characterEncoding=utf8";//文字化け防止
    final String DB_USER = "root";//ユーザーID
    final String DB_PASS = "root";//パスワード

    public List<Booth> findAll(){
    	List<Booth> boothList = new ArrayList<Booth>();

    	try {
    		Class.forName(DRIVER_NAME);
			conn = DriverManager.getConnection(DB_URL + DB_NAME+DB_ENCODE , DB_USER, DB_PASS);

			//SELECT文でboothテーブルからID,NAMEを準備
			String sql =  "SELECT guest_id,number,type FROM booth";
			PreparedStatement pStmt = conn.prepareStatement(sql);

			//SELECTを実行し、結果表を取得
			ResultSet rs = pStmt.executeQuery();

			while(rs.next()){
				String guest = rs.getString("guest_id");
				String number = rs.getString("number");
				int id = Integer.parseInt(number);
				String type = rs.getString("type");
				boothList.add(new Booth(id,type,guest));
			}
		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
    	return boothList;
    }

    //席IDから、使用可能かどうかチェックする
    public Booth usedCheck(int number){
    	Booth booth = new Booth();
    	try {
			Class.forName(DRIVER_NAME);
			conn = DriverManager.getConnection(DB_URL + DB_NAME+DB_ENCODE , DB_USER, DB_PASS);

			//SELECT文でboothテーブルからID(guest)を準備
			String sql =  "SELECT guest_id FROM booth where number=" + number;
			PreparedStatement pStmt = conn.prepareStatement(sql);

			ResultSet rs = pStmt.executeQuery();
			if(rs.next()) {
				int guest = rs.getInt("guest_id");
				if(guest>0) {
					return booth;
				}
				if(guest==0) {
					return null;
				}
			}

		} catch (ClassNotFoundException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

    	return booth;
    }

    public boolean changeGuest(int number, String guest){
    	try {
			Class.forName(DRIVER_NAME);
			conn = DriverManager.getConnection(DB_URL + DB_NAME+DB_ENCODE , DB_USER, DB_PASS);

			String sql =  "UPDATE booth SET guest_id" + guest +" where number=" + number;
			PreparedStatement pStmt = conn.prepareStatement(sql);
			int rs = pStmt.executeUpdate();

			if(rs != 1){
				return false;
			}
		} catch (ClassNotFoundException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
			return false;
		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
			return false;
		}

    	return true;
    }
}
