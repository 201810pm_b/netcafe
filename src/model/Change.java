package model;

import java.io.Serializable;

public class Change  implements Serializable {
	private int change;
	public Change() {}
	public Change(int change) {
		this.change = change;
	}

	public int getChange() {
		return change;
	}

	public void setChange(int change) {
		this.change = change;
	}

}
