package model;

import java.util.List;

import dao.StaffAccountDAO;

public class GetStaffAccountListLogic {
	public List<StaffAccount> execute(){
		StaffAccountDAO dao = new StaffAccountDAO();
		List<StaffAccount> staffAccountList = dao.findAll();
		return staffAccountList;
	}
}
