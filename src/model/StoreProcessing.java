package model;

import java.io.Serializable;

public class StoreProcessing implements Serializable {
	private String userID; //顧客ID
	private String userName; //顧客名
	private int age; //顧客の年齢
	private int userGender; //顧客の性別
	private int boothNumber; //ブース番号
	private boolean gameBooth = false; //ゲーム席
	private boolean flatBooth = false; //フラット席
	private boolean openBooth = false; //オープン席
	private boolean pairBooth = false; //ペア席
	private int entranceTime; //入店時間

	public StoreProcessing() {
	}

	public StoreProcessing(String userID, String userName, int age, int userGender, int boothNumber, boolean gameBooth,
			boolean flatBooth, boolean openBooth, boolean pairBooth) {
		this.userID = userID;
		this.userName = userName;
		this.age = age;
		this.userGender = userGender;
		this.boothNumber = boothNumber;
		this.gameBooth = gameBooth;
		this.flatBooth = flatBooth;
		this.openBooth = openBooth;
		this.pairBooth = pairBooth;
	}


	public String getUserID() {
		return userID;
	}

	public String getUserName() {
		return userName;
	}

	public int getAge() {
		return age;
	}

	public int getUserGender() {
		return userGender;
	}

	public int getBoothNumber() {
		return boothNumber;
	}

	public boolean isGameBooth() {
		return gameBooth;
	}

	public boolean isFlatBooth() {
		return flatBooth;
	}



	public boolean isOpenBooth() {
		return openBooth;
	}

	public boolean isPairBooth() {
		return pairBooth;
	}

	public int getEntranceTime() {
		return entranceTime;
	}


}
