package model;

import java.io.Serializable;

public class Guest implements Serializable{
	//フィールド
	private String id;
	private String name;
	private String enterTime;
	private String exitTime;

	//コンストラクタ
	public Guest(){	}

	public Guest(String id) {
		this.id = id;
	}

	public Guest(String id, String name) {
		this.id = id;
		this.name = name;
	}

	public Guest(String id, String name, String enterTime, String exitTime){
		this.id = id;
		this.name = name;
		this.enterTime = enterTime;
		this.exitTime = exitTime;
	}


	//getter setter
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEnterTime() {
		return enterTime;
	}

	public void setEnterTime(String enterTime) {
		this.enterTime = enterTime;
	}

	public String getExitTime() {
		return exitTime;
	}

	public void setExitTime(String exitTime) {
		this.exitTime = exitTime;
	}


}
