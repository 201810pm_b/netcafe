package model;

import java.io.Serializable;

//test
public class Booth implements Serializable {
	//フィールド
	private int number;
	private String type;//ブースタイプ（single, openなど）
	private boolean isUsed;//使用状況(使用可能、要掃除、使用中など)
	private String guest; //使用しているguestのIDを格納する

	//コンストラクタ
	public Booth(){}

	public Booth(int number){
		this.number = number;
	}

	public Booth(int number, String type) {
		this.number = number;
		this.type = type;
		this.isUsed = false;//falseのとき使用可能
	}

	public Booth(int number, String type, String guest){
		this.number = number;
		this.type = type;
		this.guest = guest;
	}

	//getter setter
	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public boolean getIsUsed() {
		return isUsed;
	}

	public void setIsUsed(boolean isUsed) {
		this.isUsed = isUsed;
	}


	public String getGuest() {
		return guest;
	}

	public void setGuest(String guest) {
		this.guest = guest;
	}

	public int feeType(String type){
		int fee= 0;

		if(type.equals("single")){
			fee = 500;

			return fee;
		}
		if(type.equals("open")){
			fee = 450;

			return fee;
		}

		return fee;
	}


	@Override
	public String toString() {
		return number+","+type+","+isUsed;
	}

	@Override
	public boolean equals(Object obj) {
		// TODO 自動生成されたメソッド・スタブ
		return super.equals(obj);
	}




}
