package model;


import java.text.SimpleDateFormat;
import java.util.Date;

public class CalcLogic {
	private int time;
	private int result;

	public CalcLogic(){}
	public Price execute(String enterDate,String exitDate){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date dateTo = null;
	    Date dateFrom = null;
	    try{
	    	//daoで取得したString型の日時を数値に変換
			dateFrom = sdf.parse(enterDate);
	        dateTo = sdf.parse(exitDate);
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
		long dateTimeTo = dateTo.getTime();
	    long dateTimeFrom = dateFrom.getTime();
	    //ミリ秒の単位を時単位に変換
		double timeSecond = (dateTimeTo - dateTimeFrom)/1000/60/60;
		//小数点以下切り上げ
		double timeHour = Math.ceil(timeSecond);
		int time = (int)timeHour;
		//代金の計算
		int result = 500 * time;
		//priceインスタンス生成
		Price price = new Price(result,time);
		return price;
	}

}
