package model;

import java.io.Serializable;

public class StaffAccount implements Serializable {
	private String id; //スタッフのID
	private String name; //スタッフの名前
	private String pass; //ログインPW

	public StaffAccount() {
	}

	public StaffAccount(String id, String name, String pass) {
		this.id = id;
		this.name = name;
		this.pass = pass;
	}

	public StaffAccount(String id, String name) {
		this.id = id;
		this.name = name;
	}

	public StaffAccount(String id) {
		this.id = id;
	}

	public String getStaffId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getPass() {
		return pass;
	}





}
