package model;

import dao.StaffAccountDAO;

public class LoginLogic {
	public boolean execute(StaffAccount staffAccount){
		if(staffAccount.getPass().equals("1234")){ //PWが1234でないとログインできない
			return true;
		}
		return false;
	}

	public boolean mainExecute(LoginStaff loginStaff){
		if(loginStaff.getPass().equals("1234")){
			return true;
		}
		return false;
	}

	//DAOのID
	public boolean idExecute(StaffAccount sAccount){
		StaffAccountDAO dao = new StaffAccountDAO();
		StaffAccount account = dao.findByLoginID(sAccount);
		return account != null;
	}
}
