package model;
import java.io.Serializable;
public class Price implements Serializable {
	private int price;
	private int time;
	public Price(){}
	public Price(int price, int time) {
		this.price = price;
		this.time = time;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public int getTime() {
		return time;
	}
	public void setTime(int time) {
		this.time = time;
	}

}
